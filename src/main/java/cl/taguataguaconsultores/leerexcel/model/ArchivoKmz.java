/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.taguataguaconsultores.leerexcel.model;

/**
 *
 * @author Acer
 */
public class ArchivoKmz extends Archivo{
    
    private String Ubicacion;
    private String Geocerca;
    private String TiempoEnPunto;

    public ArchivoKmz(String Id, 
                        String Fecha,
                        String Hora, 
                        String Code, 
                        String Latitu, 
                        String Longit, 
                        String TiempoEnPunto ,
                        String Geocerca,
                        String Ubicacion) {
        
        super(Id,Fecha,Hora,Code,Latitu,Longit);
        this.Geocerca = Geocerca;
        this.Ubicacion = Ubicacion;
        this.TiempoEnPunto = TiempoEnPunto;
    }
   
    public String getUbicacion() {
        return Ubicacion;
    }

    public void setUbicacion(String Ubicacion) {
        this.Ubicacion = Ubicacion;
    }

    public String getGeocerca() {
        return Geocerca;
    }

    public void setGeocerca(String Geocerca) {
        this.Geocerca = Geocerca;
    }

    public String getTiempoEnPunto() {
        return TiempoEnPunto;
    }

    public void setTiempoEnPunto(String TiempoEnPunto) {
        this.TiempoEnPunto = TiempoEnPunto;
    } 
}
