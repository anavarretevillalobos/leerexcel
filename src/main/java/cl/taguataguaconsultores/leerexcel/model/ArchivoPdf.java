/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.taguataguaconsultores.leerexcel.model;

/**
 *
 * @author Felipe Ramírez
 */
public class ArchivoPdf extends Archivo{
    private String TiempoM30s;
    private String TiempoM5m;
    
    private int ColorCelda; // 0 para sin pintar, 1 para rojo, 2 para amarillo

    
    public ArchivoPdf(String Id, String Fecha, String Hora, String Code, String Latitu, String Longit,String TiempoM30s, String TiempoM5m) {
        super(Id,Fecha,Hora,Code,Latitu,Longit);
        this.TiempoM30s = TiempoM30s;
        this.TiempoM5m = TiempoM5m;
    } 

    public ArchivoPdf() {
        super();
    }
    
    public String getTiempoM30s() {
        return TiempoM30s;
    }

    public String getTiempoM5m() {
        return TiempoM5m;
    }

    public int getColorCelda() {
        return ColorCelda;
    }

    public void setColorCelda(int ColorCelda) {
        this.ColorCelda = ColorCelda;
    }

    public void setTiempoM30s(String TiempoM30s) {
        this.TiempoM30s = TiempoM30s;
    }

    public void setTiempoM5m(String TiempoM5m) {
        this.TiempoM5m = TiempoM5m;
    }
        
}
