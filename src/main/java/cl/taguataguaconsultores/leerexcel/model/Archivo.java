/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.taguataguaconsultores.leerexcel.model;

/**
 *
 * @author anavarretev
 */
public class Archivo {
    
    private String Id;
    private String Fecha;
    private String Hora;
    private String Code;
    private String Latitu;
    private String Longit;

    public Archivo(){}
    
    public Archivo(String Id, String Fecha, String Hora, String Code, String Latitu, String Longit) {
        this.Id = Id;
        this.Fecha = Fecha;
        this.Hora = Hora;
        this.Code = Code;
        this.Latitu = Latitu;
        this.Longit = Longit;
    } 

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String Fecha) {
        this.Fecha = Fecha;
    }

    public String getHora() {
        return Hora;
    }

    public void setHora(String Hora) {
        this.Hora = Hora;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public String getLatitu() {
        return Latitu;
    }

    public void setLatitu(String Latitu) {
        this.Latitu = Latitu;
    }

    public String getLongit() {
        return Longit;
    }

    public void setLongit(String Longit) {
        this.Longit = Longit;
    }
    
  
    
    @Override
    public String toString() {
        return "Archivo{" + "Id=" + Id + ", Fecha=" + Fecha + ", Hora=" + Hora + ", Code=" + Code + ", Latitu=" + Latitu + ", Longit=" + Longit + '}';
    }
    
}
