/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.taguataguaconsultores.leerexcel.model;

/**
 *
 * @author Acer
 */
public class Reporte {
    private String fecha;
    private String nombreFaena;
    private String code;
    private String patenteCamion;
    private String tiempoHora;
    private String tiempoMinuto;
    private String totalPuntosGrabados;
    private String recorrido;
    
    public Reporte() {}
    
    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombreFaena() {
        return nombreFaena;
    }

    public void setNombreFaena(String nombreFaena) {
        this.nombreFaena = nombreFaena;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPatenteCamion() {
        return patenteCamion;
    }

    public void setPatenteCamion(String patenteCamion) {
        this.patenteCamion = patenteCamion;
    }

    public String getTiempoHora() {
        return tiempoHora;
    }

    public void setTiempoHora(String tiempoHora) {
        this.tiempoHora = tiempoHora;
    }

    public String getTiempoMinuto() {
        return tiempoMinuto;
    }

    public void setTiempoMinuto(String tiempoMinuto) {
        this.tiempoMinuto = tiempoMinuto;
    }

    public String getTotalPuntosGrabados() {
        return totalPuntosGrabados;
    }

    public void setTotalPuntosGrabados(String totalPuntosGrabados) {
        this.totalPuntosGrabados = totalPuntosGrabados;
    }

    public String getRecorrido() {
        return recorrido;
    }

    public void setRecorrido(String recorrido) {
        this.recorrido = recorrido;
    }
    
   
}