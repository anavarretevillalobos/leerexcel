/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.taguataguaconsultores.leerexcel;

import cl.taguataguaconsultores.leerexcel.model.Archivo;
import cl.taguataguaconsultores.leerexcel.model.ArchivoPdf;
import cl.taguataguaconsultores.leerexcel.model.Reporte;
import cl.taguataguaconsultores.leerexcel.proceso.Procesador;
import java.util.List;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


/**
 *
 * @author anavarretev
 */
public class Start {

    public static void main(String[] args) {

        try {
            /*
            //Crea Libro excel con los resultados obteneidos del CSV
            String pathFile = "C:/Users/anavarretev/Desktop/PLANILLAS_XLSX/DB/EJ.01.txt";
            String pathFileResult = "C:/Users/anavarretev/Desktop/PLANILLAS_XLSX/resultado/";
            XSSFWorkbook libroPaso1 = new XSSFWorkbook();
            XSSFSheet hojaPaso1 = libroPaso1.createSheet("Hoja1");
            
            System.out.println("Inicio Paso 1");
            
            List<Archivo> listaRows = Procesador.leerArchivoPorCodeYFecha(pathFile, "04-04-2018", "109372");
            Procesador.cargarDataExcel(libroPaso1, hojaPaso1, listaRows);
            Procesador.crearArchivoExcel(libroPaso1, "2018.04.04.109372.pc01", pathFileResult + "2018.04.04.109372.Excel/");            

            //Crear PDF con al misma informciÃ³n del excel            
            Procesador.crearArchivoPdf(listaRows, "2018.04.04.109372.pc01", pathFileResult + "2018.04.04.109372.PDF/");

            System.out.println("Fin Paso 1");
            
            System.out.println("Inicio Paso 2");
            
            List<Archivo> listaMayoresQueCeros = Procesador.filtarSoloCeros(listaRows);

            XSSFWorkbook libroPaso2 = new XSSFWorkbook();
            XSSFSheet hojaPaso2 = libroPaso2.createSheet("Hoja1");

            Procesador.cargarDataExcel(libroPaso2, hojaPaso2, listaMayoresQueCeros);
            Procesador.crearArchivoExcel(libroPaso2, "2018.04.04.109372.pc02", pathFileResult + "2018.04.04.109372.Excel/");

            //Crear PDF con al misma informción del excel            
            Procesador.crearArchivoPdf(listaMayoresQueCeros, "2018.04.04.109372.pc02", pathFileResult + "2018.04.04.109372.PDF/");            
            System.out.println("Fin Paso 2");
            
            
            System.out.println("Inicio Paso 3");
            
            XSSFWorkbook libroPaso3 = new XSSFWorkbook();
            XSSFSheet hojaPaso3 = libroPaso3.createSheet("hoja1");
            List<ArchivoPdf> listaArchivoPdf2 = Procesador.cargarDataExcelPaso3(libroPaso3, hojaPaso3, listaMayoresQueCeros);            
            Procesador.crearArchivoExcel(libroPaso3, "2018.04.04.109372.pc03", pathFileResult + "2018.04.04.109372.Excel/");

            //Crear PDF con al misma informciÃ³n del excel    
            //List<ArchivoPdf> listaArchivoPdf = Procesador.leerArchivoPaso3("2018.04.04.109372.pc03",pathFileResult + "2018.04.04.109372.Excel/");            
            Procesador.crearArchivoPdfPaso3(listaArchivoPdf2, "2018.04.04.109372.pc03", pathFileResult + "2018.04.04.109372.PDF/");
            
            // Crear archivos CSV delimitados por coma 
            Procesador.crearCSVMayores5M(listaArchivoPdf2,"2018.04.04.109372.kmz1" ,pathFileResult + "2018.04.04.109372.KMZ/");
            Procesador.crearCSVSinMayores5m(listaArchivoPdf2,"2018.04.04.109372.kmz2" ,pathFileResult + "2018.04.04.109372.KMZ/");
            System.out.println("Fin Paso 3");
            */
            /* metodo generar reporte*/
            Reporte reporte = new Reporte();   
            reporte.setFecha("2018.04.04");
            reporte.setNombreFaena("Faena Salar");
            reporte.setCode("109372");
            reporte.setPatenteCamion("HSGR71");
            reporte.setTiempoHora("2.94");
            reporte.setTiempoMinuto("176.35");
            reporte.setTotalPuntosGrabados("3052");
            String pathFileKmz = "C:/Users/anavarretev/Desktop/PLANILLAS_XLSX/resultado/56.2018.04.04.109372.KMZ/56.2018.04.04.109372.kmz1.csv";
            Procesador.crearReportePdf(reporte,pathFileKmz,"C:/Users/anavarretev/Desktop/PLANILLAS_XLSX/56.2018.04.04.109372.REPORTE/",
                                                            "C:/Users/anavarretev/Desktop/PLANILLAS_XLSX/56.2018.04.04.109372.REPORTE/56.2018.04.04.109372.rp.pdf");
            
            System.out.println("Fin Paso Reporte");
            
        } catch (Exception e) {
            System.err.println("Error al leer Excel:" + e);
        }
    }
}   
