/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.taguataguaconsultores.leerexcel.proceso;

import cl.taguataguaconsultores.leerexcel.model.Archivo;
import cl.taguataguaconsultores.leerexcel.model.ArchivoKmz;
import cl.taguataguaconsultores.leerexcel.model.ArchivoPdf;
import cl.taguataguaconsultores.leerexcel.model.HoraFormato;
import cl.taguataguaconsultores.leerexcel.model.Reporte;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author anavarretev
 */
public class Procesador {

    
    // iniico paso 1
    public static List<Archivo> leerArchivoPorCodeYFecha(String pathFile, String filterDate, String filterCode) {

        List<Archivo> listaRows = new ArrayList();

        try {
            BufferedReader bufferLectura = new BufferedReader(new FileReader(pathFile));

            // Leer una linea del archivo
            String linea = bufferLectura.readLine();
            Archivo archivo = null;
            int i = 0;
            while (linea != null) {
                if(i > 0){
                    String[] campos = linea.split(";");                
                    String Id = campos[0];
                    String[] fh = campos[1].split(" ");
                    String[] fn = fh[0].split("-");
                    String Fecha = fn[2]+"-"+fn[1]+"-"+fn[0];
                    String Hora = fh[1];
                    String Code = campos[2];
                    String Latitu = campos[3];
                    String Longit = campos[4];

                    archivo = new Archivo(Id,Fecha,Hora,Code,Latitu,Longit);

                    if (filterDate.equals(archivo.getFecha())
                            && filterCode.equals(archivo.getCode())) {
                        listaRows.add(archivo);
                    }                    
                }
                linea = bufferLectura.readLine();
                i++;
            }
        } catch (IOException io) {
            System.err.println("Error  : " + io);
        }
        return listaRows;
    }

    public static void cargarDataExcel(XSSFWorkbook libro, XSSFSheet hoja1, List<Archivo> listaRows) {

        //Cabeceras de nuevo excel.
        String[] header = new String[]{"Id", "Fecha", "Hora", "Code", "Latitu", "Longit"};
        //Poner negrita a la cabecera del nuevo excel.
        CellStyle style = libro.createCellStyle();
        Font font = libro.createFont();
        font.setBold(true);
        style.setFont(font);

        for (int i = 0; i <= listaRows.size(); i++) {
            XSSFRow row = hoja1.createRow(i);//se crea las filas
            for (int j = 0; j < header.length; j++) {
                if (i == 0) {//para la cabecera
                    XSSFCell cell = row.createCell(j);//se crea las celdas para la cabecera, junto con la posiciÃ³n
                    cell.setCellStyle(style); // se aÃ±ade el style crea anteriormente 
                    cell.setCellValue(header[j]);//se aÃ±ade el contenido					

                } else {//para el contenido
                    XSSFCell cell = row.createCell(j);//se crea las celdas para la contenido, junto con la posiciÃ³n
                    switch (j) {
                        case 0:
                            cell.setCellValue(listaRows.get(i - 1).getId());
                            break;
                        case 1:
                            cell.setCellValue(listaRows.get(i - 1).getFecha());
                            break;
                        case 2:
                            cell.setCellValue(listaRows.get(i - 1).getHora());
                            break;
                        case 3:
                            cell.setCellValue(listaRows.get(i - 1).getCode());
                            break;
                        case 4:
                            cell.setCellValue(listaRows.get(i - 1).getLatitu());
                            break;
                        case 5:
                            cell.setCellValue(listaRows.get(i - 1).getLongit());
                            break;
                        default:
                            throw new AssertionError();
                    }
                }
            }
        }
    }

    public static void crearArchivoExcel(XSSFWorkbook libro, String nameFile, String pathFile) {
        File directorio = new File(pathFile);

        if (!directorio.exists()) {
            if (directorio.mkdirs()) {
                System.out.println("Directorio Excel creado");
            } else {
                System.out.println("Error al crear directorio Excel");
            }
        }

        File file = new File(pathFile + File.separator + nameFile + ".xlsx");
        try (FileOutputStream fileOuS = new FileOutputStream(file)) {
            if (file.exists()) {// si el archivo existe se elimina
                file.delete();
                System.out.println("Archivo Excel eliminado");
            }
            libro.write(fileOuS);
            fileOuS.flush();
            fileOuS.close();
            System.out.println("Archivo Excel Creado");

        } catch (FileNotFoundException e) {
           e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void crearArchivoPdf(List<Archivo> listaRows, String nameFile, String pathFile) {
        try {

            File directorio = new File(pathFile);

            if (!directorio.exists()) {
                if (directorio.mkdirs()) {
                    System.out.println("Directorio PDF creado");
                } else {
                    System.out.println("Error al crear directorio PDF");
                }
            }

            // Se crea el documento
            Document documento = new Document();

            File file = new File(directorio + File.separator + nameFile + ".pdf");
            // El OutPutStream para el fichero donde crearemos el PDF
            FileOutputStream ficheroPDF = new FileOutputStream(file);
            if (file.exists()) {// si el archivo existe se elimina
                file.delete();
                System.out.println("Archivo PDF eliminado");
            }

            // Se asocia el documento de OutPutStream
            PdfWriter.getInstance(documento, ficheroPDF);
            // Se abre el documento
            documento.open();

            // Creamos una tabla
            PdfPTable tabla = new PdfPTable(6);
            tabla.addCell("Id");
            tabla.addCell("Fecha");
            tabla.addCell("Hora");
            tabla.addCell("Code");
            tabla.addCell("Latitu");
            tabla.addCell("Longit");

            for (int i = 0; i < listaRows.size(); i++) {
                tabla.addCell(listaRows.get(i).getId());
                tabla.addCell(listaRows.get(i).getFecha());
                tabla.addCell(listaRows.get(i).getHora());
                tabla.addCell(listaRows.get(i).getCode());
                tabla.addCell(listaRows.get(i).getLatitu());
                tabla.addCell(listaRows.get(i).getLongit());
            }
            // AÃ±adimos la tabla al documento
            documento.add(tabla);
            // Se cierra el documento
            documento.close();

        } catch (FileNotFoundException | DocumentException ex) {
            System.err.println("Error al genera PDF :" + ex);
        }
    }
    
    // fin paso 1
    
    
    // paso 2
    public static List<Archivo> filtarSoloCeros(List<Archivo> listaRows) {

        List<Archivo> listaLatitu = new ArrayList();
        double LatPrimer = 0;
        double LonPrimer = 0;
        int i = 0;
        double total = 0;
        try {
            for (Archivo ar : listaRows) {

                if (i > 0) {
                    double totalLat = LatPrimer - Double.parseDouble(ar.getLatitu());
                    double totalLon = LonPrimer - Double.parseDouble(ar.getLongit());

                    totalLat = Math.round(totalLat * 100);
                    totalLat = totalLat / 100;

                    totalLon = Math.round(totalLon * 100);
                    totalLon = totalLon / 100;

                    total = totalLon + totalLat;

                    if (total != 0) {
                        listaLatitu.add(ar);
                    }
                    LatPrimer = Double.parseDouble(ar.getLatitu());
                    LonPrimer = Double.parseDouble(ar.getLongit());
                } else {
                    LatPrimer = Double.parseDouble(ar.getLatitu());
                    LonPrimer = Double.parseDouble(ar.getLongit());
                    listaLatitu.add(ar);
                    i++;
                }                
            }
            //Se agrega el ultimo registro
            int largo = listaRows.size();
            listaLatitu.add(listaRows.get(largo -1));
            
        } catch (NumberFormatException e) {
            System.err.println("Error al quitar los valores ceros : " + e);
        }
        return listaLatitu;
    }

    // CREACION DE METODOS PARA FUNCIONALIDADES DEL APLICATIVO
    public static List<ArchivoPdf> cargarDataExcelPaso3(XSSFWorkbook libro, XSSFSheet hoja1, List<Archivo> listaRows) {
        // lista y clase para poder almacenar la variable auxiliar que son mayores a 5 min ---- se crean las cabeceras (primer registro)
        List<ArchivoPdf> listArchivoPdf = new ArrayList();
        //ArchivoPdf archPdf = new ArchivoPdf();
        try {
            //Cabeceras de nuevo excel.
            String[] header = new String[]{"Id", "Fecha", "Hora", "Code", "Latitu", "Longit", "Tiempo > 30 seg", "Tiempo > 5 min"};
            //Poner negrita a la cabecera del nuevo excel.
            CellStyle style = libro.createCellStyle();
            Font font = libro.createFont();
            font.setBold(true);
            style.setFont(font);
            // Pintar la cabecera en rojo tiempo > 30 seg
            CellStyle style2 = libro.createCellStyle();
            style2.setFillForegroundColor(IndexedColors.CORAL.index);
            style2.setFillBackgroundColor(IndexedColors.CORAL.index);
            style2.setFont(font);

            style2.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            // Pintar la cabecera en amarillo tiempo > 5 min
            CellStyle style3 = libro.createCellStyle();
            style3.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            style3.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
            style3.setFillBackgroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
            style3.setFont(font);
            ArchivoPdf archPdf2 = null;
            for (int i = 0; i <= listaRows.size(); i++) {
                XSSFRow row = hoja1.createRow(i);//se crea las filas
                archPdf2 = new ArchivoPdf();
                for (int j = 0; j < header.length; j++) {
                    if (i == 0) { //para la cabecera
                        switch (j) {
                            case 6: {
                                // si es cabecera de tiempo > 30 seg rojo
                                XSSFCell cell = row.createCell(j);//se crea las celdas para la cabecera, junto con la posiciÃ³n
                                cell.setCellStyle(style2); // se aÃ±ade el style crea anteriormente 
                                cell.setCellValue(header[j]);//se aÃ±ade el contenido
                                break;
                            }
                            case 7: {
                                // si es cabecera de tiempo > 5 min amarillo
                                XSSFCell cell = row.createCell(j);//se crea las celdas para la cabecera, junto con la posiciÃ³n
                                cell.setCellStyle(style3); // se aÃ±ade el style crea anteriormente 
                                cell.setCellValue(header[j]);//se aÃ±ade el contenido

                                break;
                            }
                            default: {
                                XSSFCell cell = row.createCell(j);//se crea las celdas para la cabecera, junto con la posiciÃ³n
                                cell.setCellStyle(style); // se aÃ±ade el style crea anteriormente 
                                cell.setCellValue(header[j]);//se aÃ±ade el contenido

                                // se trabajan las lista y objeto Archivo PDF
                                break;
                            }
                        }

                    } else {//para el contenido
                        XSSFCell cell = row.createCell(j);//se crea las celdas para la contenido, junto con la posiciÃ³n 
                        switch (j) {
                            case 0:
                                cell.setCellValue(listaRows.get(i - 1).getId());
                                archPdf2.setId(listaRows.get(i - 1).getId());
                                break;
                            case 1:
                                cell.setCellValue(listaRows.get(i - 1).getFecha());
                                archPdf2.setFecha(listaRows.get(i - 1).getFecha());
                                break;
                            case 2:
                                cell.setCellValue(listaRows.get(i - 1).getHora());
                                archPdf2.setHora(listaRows.get(i - 1).getHora());
                                break;
                            case 3:
                                cell.setCellValue(listaRows.get(i - 1).getCode());
                                archPdf2.setCode(listaRows.get(i - 1).getCode());
                                break;
                            case 4:
                                cell.setCellValue(listaRows.get(i - 1).getLatitu());
                                archPdf2.setLatitu(listaRows.get(i - 1).getLatitu());
                                break;
                            case 5:
                                cell.setCellValue(listaRows.get(i - 1).getLongit());
                                archPdf2.setLongit(listaRows.get(i - 1).getLongit());
                                break;
                            case 6:
                                if (i >= listaRows.size()) {
                                    cell.setCellValue("");
                                    archPdf2.setTiempoM30s("");
                                    break;
                                }

                                String[] hora1 = listaRows.get(i - 1).getHora().split(":");
                                String[] hora2 = listaRows.get(i).getHora().split(":");

                                LocalTime tiempo = LocalTime.of(Integer.parseInt(hora2[0]),
                                                                Integer.parseInt(hora2[1]),
                                                                Integer.parseInt(hora2[2]));

                                LocalTime tiempoFinal = tiempo.minusHours(Integer.parseInt(hora1[0])).
                                                                minusMinutes(Integer.parseInt(hora1[1])).
                                                                minusSeconds(Integer.parseInt(hora1[2]));
                                
                                
                                if(tiempoFinal.getSecond()>= 30 || 
                                    tiempoFinal.getMinute() >= 1 || 
                                    tiempoFinal.getHour() >= 1){
                                    
                                    cell.setCellStyle(style2);
                                    archPdf2.setColorCelda(1);
                                }
                                
                                cell.setCellValue(tiempoFinal.toString());
                                archPdf2.setTiempoM30s(tiempoFinal.toString());
                                archPdf2.setTiempoM5m(tiempoFinal.toString());
                                break;
                            case 7:
                                if (i == listaRows.size()) {
                                    cell.setCellValue("");
                                    archPdf2.setTiempoM5m("");
                                    break;
                                }

                                String[] hora12 = listaRows.get(i - 1).getHora().split(":");
                                String[] hora22 = listaRows.get(i).getHora().split(":");

                                LocalTime tiempo2 = LocalTime.of(Integer.parseInt(hora22[0]),
                                                                Integer.parseInt(hora22[1]),
                                                                Integer.parseInt(hora22[2]));
                                
                                LocalTime tiempoFinal2 = tiempo2.minusHours(Integer.parseInt(hora12[0])).
                                                                minusMinutes(Integer.parseInt(hora12[1])).
                                                                minusSeconds(Integer.parseInt(hora12[2]));
                                
                                
                                if(tiempoFinal2.getMinute() >= 5 || 
                                    tiempoFinal2.getHour() >= 1){
                                    
                                    cell.setCellStyle(style3);
                                    archPdf2.setColorCelda(2);
                                    archPdf2.setTiempoM5m(tiempoFinal2.toString());
                                    // se vuelve a crear celda del Id para poder pintar la celda
                                    XSSFCell cell2 = row.createCell(0);
                                    cell2.setCellValue(listaRows.get(i - 1).getId());
                                    cell2.setCellStyle(style3);                                    
                                }

                                cell.setCellValue(tiempoFinal2.toString());
                                archPdf2.setTiempoM30s(tiempoFinal2.toString());
                                break;
                            default:
                                throw new AssertionError();
                        }
                    }
                }
                if (archPdf2.getId() != null) {
                    listArchivoPdf.add(archPdf2);
                }
            }
        } catch (Exception e) {
            System.err.println("Error paso 3 :" + e);
        }
        return listArchivoPdf;
    }

    public static void crearArchivoPdfPaso3(List<ArchivoPdf> listaRows, String nameFile, String pathFile) {
        try {

            File directorio = new File(pathFile);

            if (!directorio.exists()) {
                if (directorio.mkdirs()) {
                    System.out.println("Directorio PDF creado");
                } else {
                    System.out.println("Error al crear directorio PDF");
                }
            }

            // Se crea el documento
            Document documento = new Document();

            File file = new File(directorio + File.separator + nameFile + ".pdf");
            // El OutPutStream para el fichero donde crearemos el PDF
            FileOutputStream ficheroPDF = new FileOutputStream(file);
            if (file.exists()) {// si el archivo existe se elimina
                file.delete();
                System.out.println("Archivo PDF eliminado");
            }

            // Se asocia el documento de OutPutStream
            PdfWriter.getInstance(documento, ficheroPDF);
            // Se abre el documento
            documento.open();

            // Creamos una tabla
            PdfPTable tabla = new PdfPTable(8);
          
            PdfPCell celdaId = new PdfPCell(new Paragraph("Id",
                                          FontFactory.getFont(FontFactory.HELVETICA,
                                          12,
                                          com.itextpdf.text.Font.BOLD,
                                          new BaseColor(0, 0, 0))));
            
            tabla.addCell(celdaId);
            
            PdfPCell celdaFecha = new PdfPCell(new Paragraph("Fecha",
                                          FontFactory.getFont(FontFactory.HELVETICA,
                                          12,
                                          com.itextpdf.text.Font.BOLD,
                                          new BaseColor(0, 0, 0))));
            
            tabla.addCell(celdaFecha);
            
            PdfPCell celdaHora = new PdfPCell(new Paragraph("Hora",
                                          FontFactory.getFont(FontFactory.HELVETICA,
                                          12,
                                          com.itextpdf.text.Font.BOLD,
                                          new BaseColor(0, 0, 0))));
            
            tabla.addCell(celdaHora);
            
            PdfPCell celdaCode = new PdfPCell(new Paragraph("Code",
                                          FontFactory.getFont(FontFactory.HELVETICA,
                                          12,
                                          com.itextpdf.text.Font.BOLD,
                                          new BaseColor(0, 0, 0))));
            tabla.addCell(celdaCode);
            
            PdfPCell celdaLatitu = new PdfPCell(new Paragraph("Latitu",
                                          FontFactory.getFont(FontFactory.HELVETICA,
                                          12,
                                          com.itextpdf.text.Font.BOLD,
                                          new BaseColor(0, 0, 0))));
            
            tabla.addCell(celdaLatitu);
            
            PdfPCell celdaLongit = new PdfPCell(new Paragraph("Longit",
                                          FontFactory.getFont(FontFactory.HELVETICA,
                                          12,
                                          com.itextpdf.text.Font.BOLD,
                                          new BaseColor(0, 0, 0))));
            
            tabla.addCell(celdaLongit);
            
            PdfPCell celda30Seg = new PdfPCell(new Paragraph("Tiempo > 30 seg",
                                          FontFactory.getFont(FontFactory.HELVETICA,
                                          12,
                                          com.itextpdf.text.Font.BOLD,
                                          new BaseColor(0, 0, 0))));
            
            BaseColor color30Seg = WebColors.getRGBColor("#FFB4BF");
            celda30Seg.setBackgroundColor(color30Seg);            
            tabla.addCell(celda30Seg);
            
            PdfPCell celda5Min = new PdfPCell(new Paragraph("Tiempo > 5 min",
                                          FontFactory.getFont(FontFactory.HELVETICA,
                                          12,
                                          com.itextpdf.text.Font.BOLD,
                                          new BaseColor(0, 0, 0))));
            
            BaseColor color5Min = WebColors.getRGBColor("#FFCF9E");
            celda5Min.setBackgroundColor(color5Min);            
            tabla.addCell(celda5Min);
            

            for (ArchivoPdf ls : listaRows) {
                
                if(ls.getColorCelda() == 2){
                    
                    PdfPCell cellId = new PdfPCell(new Paragraph(ls.getId(),
                                              FontFactory.getFont(FontFactory.HELVETICA,
                                              12,
                                              com.itextpdf.text.Font.BOLD,
                                              new BaseColor(0, 0, 0))));   
                    
                    BaseColor colorCellId = WebColors.getRGBColor("#FFCF9E");                    
                    cellId.setBackgroundColor(colorCellId);
                    
                    tabla.addCell(cellId);
                } else {
                    tabla.addCell(ls.getId());
                }
                
                tabla.addCell(ls.getFecha());
                tabla.addCell(ls.getHora());
                tabla.addCell(ls.getCode());
                tabla.addCell(ls.getLatitu());
                tabla.addCell(ls.getLongit());                
               
                // Mayores a 30 segundos
                if(ls.getColorCelda() == 1 || ls.getColorCelda() == 2 ){
                
                    PdfPCell cell30Seg = new PdfPCell(new Paragraph(ls.getTiempoM30s(),
                                              FontFactory.getFont(FontFactory.HELVETICA,
                                              12,
                                              com.itextpdf.text.Font.BOLD,
                                              new BaseColor(0, 0, 0)))); 
                    
                    BaseColor colorCell30Seg = WebColors.getRGBColor("#FFB4BF");
                    cell30Seg.setBackgroundColor(colorCell30Seg);
                    
                    tabla.addCell(cell30Seg);
                 
                } else{
                    tabla.addCell(ls.getTiempoM30s());
                } 
                // Mayores a 5 minutos
                if(ls.getColorCelda() == 2){
                    
                    PdfPCell cell5Min = new PdfPCell(new Paragraph(ls.getTiempoM5m(),
                                              FontFactory.getFont(FontFactory.HELVETICA,
                                              12,
                                              com.itextpdf.text.Font.BOLD,
                                              new BaseColor(0, 0, 0))));   
                    
                    BaseColor colorCell5Min = WebColors.getRGBColor("#FFCF9E");
                    cell5Min.setBackgroundColor(colorCell5Min);
                    
                    tabla.addCell(cell5Min);
                    
                } else {
                     tabla.addCell(ls.getTiempoM30s());
                }
                
            }
            // AÃ±adimos la tabla al documento
            documento.add(tabla);
            // Se cierra el documento
            documento.close();

        } catch (FileNotFoundException | DocumentException ex) {
            System.err.println("Error al genera PDF :" + ex);
        }
    }

    public static List<ArchivoPdf> leerArchivoPaso3(String nameFile, String pathFile) {
        List<ArchivoPdf> dataExcel = new ArrayList();
        ArrayList<String> id = new ArrayList();
        ArrayList<String> fecha = new ArrayList();
        ArrayList<String> hora = new ArrayList();
        ArrayList<String> code = new ArrayList();
        ArrayList<String> latitu = new ArrayList();
        ArrayList<String> longit = new ArrayList();
        ArrayList<String> tiempoM30s = new ArrayList();
        ArrayList<String> tiempoM5m = new ArrayList();
        try {
            // se lee el archivo del paso 3
            FileInputStream file = new FileInputStream(new File(pathFile + nameFile + ".xlsx"));
            XSSFWorkbook wb = new XSSFWorkbook(file);
            XSSFSheet sheet = wb.getSheetAt(0);
            XSSFRow xssfRow;
            XSSFCell xssfCell;

            int rows = sheet.getLastRowNum() + 1;
            int col = 0;

            // se recorre el archivo y se almacenan sus datos en memoria
            for (int i = 0; i <= rows; i++) {
                xssfRow = sheet.getRow(i);
                if (xssfRow == null) {
                    break;
                } else {
                    for (int j = 0; j < (col = xssfRow.getLastCellNum()); j++) {
                        // se almacenan los datos de la list <ArchivoPdf>
                        xssfCell = xssfRow.getCell(j);
                        switch (j) {
                            case 0:
                                id.add(xssfCell.toString());
                                break;
                            case 1:
                                fecha.add(xssfCell.toString());
                                break;
                            case 2:
                                hora.add(xssfCell.toString());
                                break;
                            case 3:
                                code.add(xssfCell.toString());
                                break;
                            case 4:
                                latitu.add(xssfCell.toString());
                                break;
                            case 5:
                                longit.add(xssfCell.toString());
                                break;
                            case 6:
                                tiempoM30s.add(xssfCell.toString());
                                break;
                            case 7:
                                tiempoM5m.add(xssfCell.toString());
                                break;
                        }
                    }

                }
            } // se termina de llenar los arreglos para luego agregarlos al list<ArchivoPdf>

            // se recorren los arreglos para poder unificar la informaciÃ³n en un solo arraylist  
            for (int i = 1; i < id.size(); i++) {
                ArchivoPdf archivo = new ArchivoPdf(id.get(i),
                                                    fecha.get(i),
                                                    hora.get(i),
                                                    code.get(i),
                                                    latitu.get(i),
                                                    longit.get(i),
                                                    tiempoM30s.get(i),
                                                    tiempoM5m.get(i));                
                dataExcel.add(archivo);

            }
        } catch (FileNotFoundException e) {
            System.out.println("Error al encontrar el archivo: " + e);
        } catch (IOException e) {
            System.out.println("Error inesperado: " + e);
        }
        return dataExcel;
    }

    //EncontrarMayores5M
    public static void crearCSVMayores5M(List<ArchivoPdf> listaRows, String nameFile, String pathFile) throws ParseException, IOException {

        // se crea la carpeta KMZ 
        File directorio = new File(pathFile);

        if (!directorio.exists()) {
            if (directorio.mkdirs()) {
                System.out.println("Directorio KMZ creado");
            } else {
                System.out.println("Error al crear directorio KMZ");
            }
        }
        // Se crea el archivo CSV
        File archivoCSV = new File(pathFile + nameFile + ".csv");
        archivoCSV.createNewFile();
        try (FileWriter writer = new FileWriter(pathFile + nameFile + ".csv")) {
            writer.write("Id,Fecha,Hora,Code,Latitu,Longit,Tiempo en Punto\n");
            //Cabeceras de nuevo excel.
            
            for (int i = 0; i < listaRows.size(); i++) {
                int colorCelda = listaRows.get(i).getColorCelda();
                if (colorCelda == 2) {
                    String id = listaRows.get(i).getId();
                    String fecha = listaRows.get(i).getFecha();
                    String hora = listaRows.get(i).getHora();
                    String code = listaRows.get(i).getCode();
                    String latitu = listaRows.get(i).getLatitu();
                    String longit = listaRows.get(i).getLongit();
                    String tiempoPunto = listaRows.get(i).getTiempoM5m();
                    //  se comienza a almacenar los datos en el archivo CSV
                    try {
                        writer.write(id + "," + fecha + "," + hora + "," + code + "," + latitu + "," + longit + "," + tiempoPunto + "\n");
                        
                    } catch (IOException e) {
                        // Error al crear el archivo, por ejemplo, el archivo
                        // estÃ¡ actualmente abierto.
                        System.out.println("Error al editar archivo CSV: " + e);
                        e.printStackTrace();
                    }
                }
            }
            writer.flush();
        }
    }

    // quitar lo mayores a 5 minutos del archivo
    public static void crearCSVSinMayores5m(List<ArchivoPdf> listaRows, String nameFile, String pathFile) throws ParseException, IOException {

        // se crea la carpeta KMZ 
        File directorio = new File(pathFile);

        if (!directorio.exists()) {
            if (directorio.mkdirs()) {
                System.out.println("Directorio KMZ creado");
            } else {
                System.out.println("Error al crear directorio KMZ");
            }
        }
        // Se crea el archivo CSV
        File archivoCSV = new File(pathFile + nameFile + ".csv");
        archivoCSV.createNewFile();
        FileWriter writer = new FileWriter(pathFile + nameFile + ".csv");
        writer.write("Id,Fecha,Hora,Code,Latitu,Longit\n");
        //Cabeceras de nuevo excel.
        int col = 8;
        for (int i = 0; i < listaRows.size(); i++) {
            int colorCelda = listaRows.get(i).getColorCelda();
            if (colorCelda != 2) {
                String id = listaRows.get(i).getId();
                String fecha = listaRows.get(i).getFecha();
                String hora = listaRows.get(i).getHora();
                String code = listaRows.get(i).getCode();
                String latitu = listaRows.get(i).getLatitu();
                String longit = listaRows.get(i).getLongit();
                // acÃ¡ se comienza a almacenar los datos en el archivo CSV
                try {
                    writer.write(id + "," + fecha + "," + hora + "," + code + "," + latitu + "," + longit + "\n");
                   
                } catch (IOException e) {
                    // Error al crear el archivo, por ejemplo, el archivo 
                    // estÃ¡ actualmente abierto.
                    System.out.println("Error al editar archivo CSV: " + e);
                    e.printStackTrace();
                }
            }
        }
        writer.flush();
        writer.close();
    }

    public static String[] crearReportePdf(Reporte reporte, String pathFile,String pathResult,String pathFileReporte) throws FileNotFoundException, DocumentException, Exception {
        
        String[] result =  new String[2];
        result[0] = "00:00:00";
        result[1] = "0.0";
        File directorio = new File(pathResult);

        if (!directorio.exists()) {
            if (directorio.mkdirs()) {
                System.out.println("Directorio REPORTE creado");
            } else {
                System.out.println("Error al crear directorio REPORTE");
            }
        }
        
        // Creacion del documento con los margenes
        Document document = new Document(PageSize.A4, 35, 30, 50, 50);

        // El archivo pdf que vamos a generar
        File file = new File(pathFileReporte);//"C:/Users/Acer/Desktop/Proyectos Alexis/PLANILLAS_XLSX/resultado/2018.04.04.109372.REPORTE/2018.04.04.109372.rp.pdf"
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        
        if(file.exists()){
            file.delete();
        }
        // Obtener la instancia del PdfWriter
        PdfWriter.getInstance(document, fileOutputStream);

        try {
            
            int index = reporte.getTiempoHora().indexOf(".");
            String hora = reporte.getTiempoHora().substring(0, index);
            
            //Obtengo los minutos con regla de tres.            
            String minutos = reporte.getTiempoHora().substring(index + 1,reporte.getTiempoHora().length());
            
            int minutoReal = (Integer.parseInt(minutos) * 60) / 100; 
            
            String formatoHora = String.format("%02d",Integer.parseInt(hora));
            // Abrir el documento            
            document.open();
            document.add(new Paragraph("Reporte y Archivo Kmz: " + reporte.getRecorrido() +"."+reporte.getFecha() + "." + reporte.getCode(),
                    FontFactory.getFont(FontFactory.HELVETICA,
                            14,
                            com.itextpdf.text.Font.UNDERLINE,
                            new BaseColor(0, 0, 0))));
            document.add(new Paragraph("\n"));
            String fet = reporte.getFecha().replace(".", "-");
            String[] fechFormato = fet.split("-");
            
            Paragraph parrafo1 = new Paragraph("Dentro de la Planilla “datos cantrack 1“, entregada por INVERSOL, en la cual se encuentran los "
                    + "datos generales de recorridos diarios de los distintos camiones y faenas, encontramos el registro "
                    + "hecho con fecha " +fechFormato[2]+"-"+fechFormato[1]+"-"+fechFormato[0]+", en " + reporte.getNombreFaena() + ", con el código " + reporte.getCode() + " que identifica el camión "
                    + "patente " + reporte.getPatenteCamion() + ", en la que se establece un tiempo exceso en minutos de " + reporte.getTiempoMinuto() + " equivalentes a "
                    + reporte.getTiempoHora() +" horas lo que corresponde en términos horarios a " + hora + " horas " + String.valueOf(minutoReal) + " minutos.");
            parrafo1.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(parrafo1);

            document.add(new Paragraph("\n"));

            Paragraph parrafo2 = new Paragraph("Es decir INVERSOL reclama que en este recorrido dicho camión permaneció dentro de Geocerca en "
                    + reporte.getNombreFaena() + "  por un tiempo de exceso de: " + formatoHora +":"+ minutoReal +":00 horas.");
            parrafo2.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(parrafo2);
            document.add(new Paragraph("\n"));

            Paragraph parrafo3 = new Paragraph("El presente reporte entrega los detalles de la ubicación y tiempo de espera en este recorrido "
                    + "diario. La totalidad de puntos grabados por el sistema de control GPS instalado en dicho camión "
                    + "corresponden a " + reporte.getTotalPuntosGrabados() +", todos los cuales fueron procesados.");
            parrafo3.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(parrafo3);
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("Fecha " +fechFormato[2]+"-"+fechFormato[1]+"-"+fechFormato[0]+", código camión " + reporte.getCode() + ", patente " + reporte.getPatenteCamion() + ", en " + reporte.getNombreFaena() + "."));
            document.add(new Paragraph("\n"));
            
            /// tabla del reporte "Fuera de la Geocerca"            
            List<ArchivoKmz> archKmz = Procesador.leerArchivoKmz(pathFile);
            List<ArchivoKmz> tbF = new ArrayList<>();
            List<ArchivoKmz> tbD = new ArrayList<>();
            
            for(ArchivoKmz ar : archKmz){
                switch(ar.getGeocerca()){
                    case "F": tbF.add(ar);
                        break;
                    case "D": tbD.add(ar);
                        break;
                    default: break;                
                }
                    
            }

            
            PdfPTable tabla = new PdfPTable(8);
            PdfPTable tabla2 = new PdfPTable(8);
             PdfPCell celdaId = new PdfPCell(new Paragraph("Id",
                                            FontFactory.getFont(FontFactory.HELVETICA,
                                            12,
                                            com.itextpdf.text.Font.BOLD,
                                            new BaseColor(0, 0, 0))));
             PdfPCell celdaFecha = new PdfPCell(new Paragraph("Fecha",
                                                FontFactory.getFont(FontFactory.HELVETICA,
                                                12,
                                                com.itextpdf.text.Font.BOLD,
                                                new BaseColor(0, 0, 0))));   
            PdfPCell celdaHora = new PdfPCell(new Paragraph("Hora",
                                                FontFactory.getFont(FontFactory.HELVETICA,
                                                12,
                                                com.itextpdf.text.Font.BOLD,
                                                new BaseColor(0, 0, 0)))); 
             PdfPCell celdaCode = new PdfPCell(new Paragraph("Code",
                                                FontFactory.getFont(FontFactory.HELVETICA,
                                                12,
                                                com.itextpdf.text.Font.BOLD,
                                                new BaseColor(0, 0, 0))));
             PdfPCell celdaLatitu = new PdfPCell(new Paragraph("Latitu",
                                                FontFactory.getFont(FontFactory.HELVETICA,
                                                12,
                                                com.itextpdf.text.Font.BOLD,
                                                new BaseColor(0, 0, 0))));
             PdfPCell celdaLongit = new PdfPCell(new Paragraph("Longit",
                                                FontFactory.getFont(FontFactory.HELVETICA,
                                                12,
                                                com.itextpdf.text.Font.BOLD,
                                                new BaseColor(0, 0, 0))));
            PdfPCell celdaTiempoPunto = new PdfPCell(new Paragraph("Tiempo en punto",
                                                FontFactory.getFont(FontFactory.HELVETICA,
                                                12,
                                                com.itextpdf.text.Font.BOLD,
                                                new BaseColor(0, 0, 0))));
            PdfPCell celdaUbicacion = new PdfPCell(new Paragraph("Ubicación",
                                                FontFactory.getFont(FontFactory.HELVETICA,
                                                12,
                                                com.itextpdf.text.Font.BOLD,
                                                new BaseColor(0, 0, 0))));
            
            if(tbF.size() > 0){
                tabla.setHorizontalAlignment(Element.ALIGN_LEFT);
                tabla.setWidthPercentage(100);
                tabla.addCell(celdaId);
                tabla.addCell(celdaFecha);
                tabla.addCell(celdaHora);
                tabla.addCell(celdaCode);
                tabla.addCell(celdaLatitu);
                tabla.addCell(celdaLongit);            
                tabla.addCell(celdaTiempoPunto);
                tabla.addCell(celdaUbicacion);
            }
            if(tbD.size() > 0){
                tabla2.setHorizontalAlignment(Element.ALIGN_LEFT);
                tabla2.setWidthPercentage(100);
                tabla2.addCell(celdaId);
                tabla2.addCell(celdaFecha);
                tabla2.addCell(celdaHora);
                tabla2.addCell(celdaCode);
                tabla2.addCell(celdaLatitu);
                tabla2.addCell(celdaLongit);
                tabla2.addCell(celdaTiempoPunto);
                tabla2.addCell(celdaUbicacion);
            }
            
            List<HoraFormato> listTiempoF = new ArrayList<>();
            List<HoraFormato> listTiempoD = new ArrayList<>();
            
            for(ArchivoKmz kmz : archKmz) {
                if ("F".equals(kmz.getGeocerca())) {
                    tabla.addCell(kmz.getId());
                    tabla.addCell(kmz.getFecha());
                    tabla.addCell(kmz.getHora());
                    tabla.addCell(kmz.getCode());
                    tabla.addCell(kmz.getLatitu());
                    tabla.addCell(kmz.getLongit());                    
                    
                    String[] horas = new String[3];
                    String[] hhF1 = kmz.getTiempoEnPunto().split(":");
                    switch (hhF1.length) {
                        case 1:
                            horas[0] = hhF1[0];
                            horas[1] = "00";
                            horas[2] = "00";
                            break;
                        case 2:
                            horas[0] = hhF1[0];
                            horas[1] = hhF1[1];
                            horas[2] = "00";
                            break;
                        default:
                            horas[0] = hhF1[0];
                            horas[1] = hhF1[1];
                            horas[2] = hhF1[2];                            
                            break;
                    }
                    HoraFormato tiempo = new HoraFormato();
                    tiempo.setHora(Integer.parseInt(horas[0]));
                    tiempo.setMinuto(Integer.parseInt(horas[1]));
                    tiempo.setSegundo(Integer.parseInt(horas[2]));
                    listTiempoF.add(tiempo);
                    
                    tabla.addCell(horas[0]+":"+horas[1]+":"+horas[2]);
                    tabla.addCell(kmz.getUbicacion());
                    
                    
                }else if ("D".equals(kmz.getGeocerca())){
                    tabla2.addCell(kmz.getId());
                    tabla2.addCell(kmz.getFecha());
                    tabla2.addCell(kmz.getHora());
                    tabla2.addCell(kmz.getCode());
                    tabla2.addCell(kmz.getLatitu());
                    tabla2.addCell(kmz.getLongit());
                    
                    String[] horas = new String[3];
                    String[] hhF1 = kmz.getTiempoEnPunto().split(":");
                    switch (hhF1.length) {
                        case 1:
                            horas[0] = hhF1[0];
                            horas[1] = "00";
                            horas[2] = "00";
                            break;
                        case 2:
                            horas[0] = hhF1[0];
                            horas[1] = hhF1[1];
                            horas[2] = "00";
                            break;
                        default:
                            horas[0] = hhF1[0];
                            horas[1] = hhF1[1];
                            horas[2] = hhF1[2];
                            break;
                    }
                    
                    
                    HoraFormato tiempo = new HoraFormato();
                    tiempo.setHora(Integer.parseInt(horas[0]));
                    tiempo.setMinuto(Integer.parseInt(horas[1]));
                    tiempo.setSegundo(Integer.parseInt(horas[2]));
                    listTiempoD.add(tiempo);  
                    
                    tabla2.addCell(horas[0]+":"+horas[1]+":"+horas[2]);
                    tabla2.addCell(kmz.getUbicacion());
                }                
            }
              
            
            document.add(new Paragraph("Fuera de la Geocerca: \n",
                        FontFactory.getFont(FontFactory.HELVETICA,
                                12,
                                com.itextpdf.text.Font.UNDERLINE,
                                new BaseColor(0, 0, 0))));

            
            if(tbF.size() > 0){
                LocalTime tiempoF =  LocalTime.of(listTiempoF.get(0).getHora(),
                                                listTiempoF.get(0).getMinuto(),
                                                listTiempoF.get(0).getSegundo());            
                int hhF = 0;
                int mmF = 0;
                int sgF = 0;

                for (int i = 1; i < listTiempoF.size(); i++) {
                    hhF = listTiempoF.get(i).getHora() + hhF;
                    mmF = listTiempoF.get(i).getMinuto() + mmF;
                    sgF =  listTiempoF.get(i).getSegundo() + sgF;
                }

                LocalTime tiempoFIter =  tiempoF.plusHours(hhF)
                                            .plusMinutes(mmF)
                                            .plusSeconds(sgF);


                                // Añadimos la tabla al documento
                document.add(new Paragraph("\n"));
                document.add(tabla);
                document.add(new Paragraph("\n"));
                document.add(new Paragraph("\n"));
                document.add(new Paragraph("Total de Tiempos detenido fuera de la Geocerca: " + tiempoFIter.toString() + " horas"));
                //11:41:58
                // Pasa a a la pagina siguiente.
                if(tbD.size() > 0){
                    document.newPage();
                }
            } else{
                document.add(new Paragraph("\n"));
                document.add(new Paragraph("Total de Tiempos detenido fuera de la Geocerca: 00:00:00 horas"));
                document.add(new Paragraph("\n"));
            } 
            /// tabla del reporte "Dentro de la geocerca"
            document.add(new Paragraph("Dentro de la Geocerca: \n",
                    FontFactory.getFont(FontFactory.HELVETICA,
                            12,
                            com.itextpdf.text.Font.UNDERLINE,
                            new BaseColor(0, 0, 0))));
            
            if(tbD.size() > 0){
                // Añadimos la tabla al documento
                document.add(new Paragraph("\n"));
                document.add(tabla2);
                document.add(new Paragraph("\n"));
                document.add(new Paragraph("\n"));

                 LocalTime tiempoD =  LocalTime.of(listTiempoD.get(0).getHora(),
                                                listTiempoD.get(0).getMinuto(),
                                                listTiempoD.get(0).getSegundo());

                int hhD = 0;
                int mmD = 0;
                int sgD = 0;
                for (int i = 1; i < listTiempoD.size(); i++) {
                    hhD = listTiempoD.get(i).getHora() + hhD;
                    mmD = listTiempoD.get(i).getMinuto() + mmD;
                    sgD =  listTiempoD.get(i).getSegundo() + sgD;
                }

                LocalTime tiempoDIter =  tiempoD.plusHours(hhD)
                                            .plusMinutes(mmD)
                                            .plusSeconds(sgD);


                int hhFinal = tiempoDIter.getHour();
                int mmFinal = (tiempoDIter.getMinute() * 100) / 60;
                
                String tiempoFinal = hhFinal +"." + String.format("%02d",mmFinal);
                result[0] = tiempoDIter.toString();
                result[1] = tiempoFinal;
                document.add(new Paragraph("Total de Tiempos detenido dentro de la Geocerca: " + tiempoDIter.toString() + " horas, Equivalente a " + tiempoFinal + " horas."));
            } else {                
                document.add(new Paragraph("\n"));
                document.add(new Paragraph("Total de Tiempos detenido dentro de la Geocerca: 00:00:00 horas, Equivalente a 00.00 horas"));
            }
            
            
            document.close();
            System.out.println("Si se creo el pdf");
        } catch (Exception e) {
            System.out.println("No se pudo crear parrafo:" + e);
            throw new Exception(e.getMessage());
        }
        return result;
    }

    public static List<ArchivoKmz> leerArchivoKmz(String pathFile) {
        List<ArchivoKmz> listaRows = new ArrayList();

        try {
            BufferedReader bufferLectura = new BufferedReader(new FileReader(pathFile));

            // Leer una linea del archivo
            String linea = bufferLectura.readLine();
            ArchivoKmz archivoKmz = null;
            int i = 0;
            while (linea != null) {
                // Sepapar la linea leida con el separador definido previamente
                String[] campos = linea.split(";");
                if(campos.length == 1){
                 campos = linea.split(",");   
                }
               if(i > 0){
                    archivoKmz = new ArchivoKmz(campos[0],
                                                campos[1],
                                                campos[2],
                                                campos[3],
                                                campos[4],
                                                campos[5],
                                                campos[6],
                                                campos[7],
                                                campos[8]);
                    listaRows.add(archivoKmz);
               }
                linea = bufferLectura.readLine();
                i++;
            }
        } catch (IOException io) {
            System.err.println("Error  : " + io);
        }
        return listaRows;
    }

}
